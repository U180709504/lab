public class MyTime {
    private int hour, minute;

    public MyTime(int hour , int minute) {
        this.hour = hour;
        this.minute = minute;
    }


    public String toString() {

        return (hour < 10 ? "0" : "" )+ hour +":"+ (minute < 10 ? "0" : "" )+ minute;
    }


    public int incrementHour(int value) {
        int daydiff = (hour + value) / 24;
        int newHour = (hour + value) % 24;
        if(hour + value < 0){
            daydiff--;
            newHour = newHour + 24;
        }
        hour = newHour ;
        return daydiff;
    }




    public int incrementMinute(int value) {
        int hourdiff = (minute + value) / 60;
        minute = (minute + value) % 60;
        if((minute + value) < 0) {
            hourdiff--;
            minute += 60;
        }
        return incrementHour(hourdiff);
    }
    public boolean isBefore(MyTime anotherTime){
        int a = Integer.parseInt(toString().replaceAll(":", "")) ;
        int b = Integer.parseInt(anotherTime.toString().replaceAll(":", ""));
        return a < b;
    }

    public boolean isAfter(MyTime anotherTime) {
        int a = Integer.parseInt(toString().replaceAll(":", "")) ;
        int b = Integer.parseInt(anotherTime.toString().replaceAll(":", ""));
        return a > b;
    }

    public int minuteDifference(MyTime anotherTime) {
        return (hour * 60 + minute) - (anotherTime.hour * 60 + anotherTime.minute);
    }
}
