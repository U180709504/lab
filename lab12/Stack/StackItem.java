package Stack;

public class StackItem {
    private Object item;
    private StackItem next;

    public StackItem(Object item) {
        this.item = item;
    }

    public void setNext(StackItem next) {
        this.next = next;
    }

    public Object getItem() {
        return item;
    }

    public StackItem getNext() {
        return next;
    }
}
