package Stack;

public class StackImpl implements Stack {
    StackItem top;


    @Override
    public void push(Object item) {
        StackItem previousTop = top;
        top = new StackItem(item);
        top.setNext(previousTop);
    }

    @Override
    public Object pop() {
        StackItem previousTop = top.getNext();
        StackItem item = top;
        top = previousTop;
        return item.getItem();
    }

    @Override
    public boolean empty() {
        return top == null;
    }
}
