import Stack.Stack;
import Stack.StackImpl;
import Stack.StackArrayListImpl;

public class TestStack {
    public static void main(String[] args) {
        Stack stack = new StackArrayListImpl();
        stack.push("A");
        stack.push("B");
        stack.push("C");
        stack.push("D");

        while (!stack.empty()) {
            System.out.println(stack.pop());
        }
    }
}
