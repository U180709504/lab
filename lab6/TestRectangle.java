public class TestRectangle {
    public static void main(String[] args) {
        Point p = new Point(3,7);
        Rectangle r = new Rectangle(5,6, p);
        System.out.println("r area = " + r.area());
        System.out.println("r perimeter = " + r.perimeter());
        Point[] corners = r.corners();
        for (int i = 0; i<corners.length ; i++){
            System.out.println("x = " + corners[i].xCoord + " y = " + corners[i].yCoord);
        }




        Rectangle r2 = new Rectangle(7,9, new Point(3,10));
        System.out.println("r2 area = " + r2.area());
        System.out.println("r2 perimeter = " + r2.perimeter());

    }
}
