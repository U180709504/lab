package tictactoe;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);

        Board board = new Board();

        System.out.println(board);
        while (!board.isEnded()) {

            int player = board.getCurrentPlayer();
            int row = 0;
            boolean isNumber = true;
            do {
                try {
                    System.out.print("Player " + player + " enter row number:");
                    row = Integer.valueOf(reader.nextLine());
                    System.out.println("Input is a number");
                    isNumber = true;
                } catch (NumberFormatException nfe) {
                    System.out.println("Invalid Number");
                    isNumber = false;
                }
            }while(!isNumber);
            int col = 0;
            do {
                try {
                    System.out.print("Player " + player + " enter column number:");
                    col = Integer.valueOf(reader.nextLine());
                    isNumber = true;
                } catch (NumberFormatException nfe) {
                    System.out.println("Invalid Number");
                    isNumber = false;
                }
            }while (!isNumber);
            try {
                board.move(row, col);board.move(row, col);
                System.out.println(board);
            }catch (InvalidMoveException ex){
                System.out.println(ex.getMessage());
            }


        }


        reader.close();
    }


}
